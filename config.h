/* See LICENSE file for copyright and license details. */

#include <X11/XF86keysym.h>

/* appearance */
static const unsigned int borderpx  = 3;        /* border pixel of windows */
static const unsigned int gappx     = 0;        /* gaps between windows */
static const unsigned int snap      = 5;       /* snap pixel */
static const unsigned int systraypinning = 0;   /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayspacing = 2;   /* systray spacing */
static const int systraypinningfailfirst = 1;   /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const int showsystray        = 1;     /* 0 means no systray */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = { "Fira Code:size=11" };
static const char dmenufont[]       = "Fira Code:size=11";

static const char col_gray1[]       = "#000000";
static const char col_gray2[]       = "#313333";
static const char col_gray3[]       = "#999999";
static const char col_gray4[]       = "#eeeeee";
static const char col_cyan[]        = "#005577";
static const char col_bg[]          = "#1b1918";
static const char col_fg[]          = "#f1efee";
static const char col_grey[]        = "#a8a19f";
static const char col_blue[]        = "#407ee7";

static const char color00[] = "#303030";  // black
static const char color01[] = "#c23b2d";  // red
static const char color02[] = "#7FF088";  // green
static const char color03[] = "#e7940f";  // yellow
static const char color04[] = "#156ADF";  // blue
static const char color05[] = "#9442b6";  // magenta
static const char color06[] = "#12a689";  // cyan
static const char color07[] = "#a9a9a9";  // white
static const char color08[] = "#4d4d4d";  // black b
static const char color09[] = "#ec4c3c";  // red b
static const char color10[] = "#30d052";  // green b
static const char color11[] = "#f3c714";  // yellow b
static const char color12[] = "#0b4f79";  // blue b
static const char color13[] = "#a75ec5";  // magenta b
static const char color14[] = "#0ecca7";  // cyan b
static const char color15[] = "#d3d3d3";  // white b

static const char magentadark[] =	"#371347";
static const char magenta[] =		"#ffffff";
// static const char magenta[] =		"#9442b6";
static const char black[] =		"#000000";
static const char white[] =		"#d3d3d3";
static const char gray[] =		"#999999";

static const unsigned int baralpha = 0xff;
static const unsigned int borderalpha = OPAQUE;
static const char *colors[][3]      = {
	/*               fg		bg	border   */
	[SchemeNorm] = { gray,		black,	black},
	[SchemeSel]  = { magenta,	black,	gray},
};
static const unsigned int alphas[][3]      = {
	/*               fg      bg        border     */
	[SchemeNorm] = { OPAQUE, baralpha, borderalpha },
	[SchemeSel]  = { OPAQUE, baralpha, borderalpha },
};

/* tagging */
static const char *tags[] = { "W", "T", "3", "4", "5", "6", "7", "8", "M", "S" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      instance    title       tags mask     isfloating   monitor */
	{ "Firefoxxxx",  NULL,       NULL,        1 << 8,       0,           -1 },
	{ "Peek",  NULL,       NULL,             0,       1,           -1 },
};

/* layout(s) */
static const float mfact     = 0.65; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */
static int attachbelow = 1;    /* 1 means attach after the currently active window */

#include "fibonacci.c"
static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[t]",      tile },    /* first entry is default */
	{ "[f]",      NULL },    /* no layout function means floating behavior */
	{ "[m]",      monocle },
 	{ "[@]",      spiral },
 	{ "[\\]",     dwindle },
	{ "[r]",      rev_tile },    /* first entry is default */
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */

static const char *dmenucmd[] = { "dmenu_run_history", "-m", dmenumon, "-fn", dmenufont, "-nb", black, "-nf", gray, "-sb", black, "-sf", magenta, NULL };
static const char *termcmd[]  = { "kitty", NULL };

static const char *upbrig[]   = { "light", "-A", "10",     NULL };
static const char *downbrig[] = { "light", "-U", "10",     NULL };

static const char *playern[] = { "playerctl", "next",       NULL};
static const char *playerb[] = { "playerctl", "play-pause", NULL};
static const char *playerp[] = { "playerctl", "previous",   NULL};
static const char *players[] = { "playerctl", "stop",   NULL};


#include "movestack.c"
static Key keys[] = {
	/* modifier                     key        function        argument */
	{ 0,                            XF86XK_MonBrightnessUp,     spawn, {.v = upbrig   } },
	{ 0,                            XF86XK_MonBrightnessDown,   spawn, {.v = downbrig } },
	{ 0,                            XF86XK_AudioLowerVolume,    spawn, SHCMD("amixer -q sset Master 5%- unmute") },
	{ 0,                            XF86XK_AudioRaiseVolume,    spawn, SHCMD("amixer -q sset Master 5%+ unmute") },
	{ 0,                            XF86XK_AudioMute,           spawn, SHCMD("amixer -q sset Master toggle")},
	{ 0,                            XF86XK_AudioMicMute,        spawn, SHCMD("amixer set Capture toggle") },
	{ 0,                            XF86XK_AudioNext,           spawn, {.v = playern} },
	{ 0,                            XF86XK_AudioPause,          spawn, {.v = playerb} },
	{ 0,                            XF86XK_AudioPrev,           spawn, {.v = playerp} },
	{ 0,                            XF86XK_AudioStop,           spawn, {.v = players} },
	{ MODKEY,                       XK_Print,   spawn,          SHCMD("maim pic-full-$(date '+%y%m%d-%H%M-%S').png") },
	{ 0,                            XK_Print,   spawn,          SHCMD("flameshot gui") },
	{ MODKEY,                       XK_n,       spawn,          {.v = playern} },
	{ MODKEY,                       XK_b,       spawn,          {.v = playerb} },
	{ MODKEY,                       XK_p,       spawn,          {.v = playerp} },
	// { MODKEY,                       XK_d,       spawn,          SHCMD("rofi -show run -i -font 'Fira Code 16'") },
	{ MODKEY,                       XK_d,       spawn,          {.v = dmenucmd} },
	{ Mod1Mask,                     XK_space,   spawn,          SHCMD("rofimoji --rofi-args=\"-i -font 'Fira Code 16'\"") },
	{ MODKEY|ShiftMask,             XK_Return,  spawn,          {.v = termcmd  } },
	{ MODKEY,                       XK_Return,  zoom,           {0} },
	{ MODKEY,                       XK_j,       focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,       focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_o,       incnmaster,     {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_o,       incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,       setmfact,       {.f = -0.01} },
	{ MODKEY,                       XK_l,       setmfact,       {.f = +0.01} },
	{ MODKEY,                       XK_Tab,     setlayout,      {0} },
	{ MODKEY|ControlMask,           XK_c,       spawn,          SHCMD("colorpicker --short --one-shot | tr -d '\n' | xclip -selection clipboard") },
	{ MODKEY,                       XK_s,       togglesticky,   {0} },
	{ MODKEY|ShiftMask,             XK_space,   togglefloating, {0} },
	{ MODKEY,                       XK_comma,   focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period,  focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,   tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period,  tagmon,         {.i = +1 } },
    { MODKEY|ShiftMask,             XK_j,      movestack,       {.i = +1 } },
    { MODKEY|ShiftMask,             XK_k,      movestack,       {.i = -1 } },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	TAGKEYS(                        XK_0,                      9)
	{ MODKEY|ShiftMask,             XK_q,      killclient,     {0} },

	{ MODKEY,                       XK_y,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_r,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY|ShiftMask,             XK_r,      setlayout,      {.v = &layouts[5]} },
	{ MODKEY|ShiftMask,             XK_y,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_u,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY|ShiftMask,             XK_u,      setlayout,      {.v = &layouts[3]} },
	{ MODKEY,                       XK_i,      setlayout,      {.v = &layouts[4]} },
	{ MODKEY|ShiftMask,             XK_i,      setlayout,      {.v = &layouts[5]} },
	{ MODKEY|ShiftMask,             XK_f,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_f,      togglefullscr,  {0} },
	{ MODKEY,                       XK_minus,  setgaps,        {.i = -1 } },
	{ MODKEY,                       XK_equal,  setgaps,        {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_equal,  setgaps,        {.i = 0  } },
	{ MODKEY|ShiftMask|ControlMask, XK_Tab,    toggleAttachBelow,     {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

